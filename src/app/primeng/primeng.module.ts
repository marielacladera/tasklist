import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CardModule} from 'primeng/card';
import { CheckboxModule} from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MenuModule } from 'primeng/menu';
import { PanelModule } from 'primeng/panel';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    CalendarModule,
    CardModule,
    CheckboxModule,
    DialogModule,
    PanelModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    MenuModule,
    ToastModule,
  ],
  providers: [MessageService],
})
export class PrimengModule { }
