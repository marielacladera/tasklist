import {Component, OnInit} from '@angular/core';
import {TaskResponse} from './responses/task.response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {
}
