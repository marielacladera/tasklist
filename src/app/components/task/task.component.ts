import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Message } from 'primeng/api';
import { ChangePriorityTask } from 'src/app/inputs/change-priority-task';
import { ChangePriorityTaskService } from 'src/app/services/change-priority-task.service';
import { CompleteTaskService } from 'src/app/services/complete-task.service';
import { DeleteTaskService } from 'src/app/services/delete-task.service';
import { ListTaskByIdService } from 'src/app/services/list-task-by-id.service';
import { ListTasksService } from 'src/app/services/list-tasks.service';
import { TaskResponse } from '../../responses/task.response';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskComponent {
  @Input() public task!: TaskResponse;

  public open: boolean;

  constructor(
    private _changePriorityService: ChangePriorityTaskService,
    private _completeTaskService: CompleteTaskService,
    private _listTaskByIdService: ListTaskByIdService,
    private _deleteTaskService: DeleteTaskService,
    private _listTasksService: ListTasksService
  ) {
    this.open = false;
  }

  public completeTask(id: number): void {
    this._completeTaskService
      .completeTask(id)
      .pipe(take(1))
      .subscribe(() => {
        this._listTasksService
          .list()
          .pipe(take(1))
          .subscribe((tasks: TaskResponse[]) => {
            const message: Message = {
              severity: 'success',
              summary: 'Success',
              detail: `The task with id ${id} was to complet with exit`,
            };
            this._listTasksService.setChangeTask(tasks);
            this._listTasksService.setChangeMessage(message);
          });
      });
  }

  public deleteTask(id: number): void {
    this._deleteTaskService
      .delete(id)
      .pipe(take(1))
      .subscribe(() => {
        this._listTasksService
          .list()
          .pipe(take(1))
          .subscribe((tasks: TaskResponse[]) => {
            const message: Message = {
              severity: 'success',
              summary: 'Success',
              detail: `The task with id ${id} was to delete with exit`,
            };
            this._listTasksService.setChangeTask(tasks);
            this._listTasksService.setChangeMessage(message);
          });
      });
  }

  public changePriorityTask(id: number): void {
    this._listTaskByIdService
      .listById(id)
      .pipe(take(1))
      .subscribe((task: TaskResponse) => {
        let instance: ChangePriorityTask = this._createInstanceChangePriority(
          id,
          task.isPriority
        );
        this._changePriorityService
          .changePriority(instance)
          .pipe(take(1))
          .subscribe(() => {
            this._listTasksService
              .list()
              .pipe(take(1))
              .subscribe((tasks: TaskResponse[]) => {
                const message: Message = {
                  severity: 'success',
                  summary: 'Success',
                  detail: `The task with id ${id} was to change the priority with exit`,
                };
                this._listTasksService.setChangeTask(tasks);
                this._listTasksService.setChangeMessage(message);
              });
          });
      });
  }

  private _createInstanceChangePriority(
    id: number,
    priority: boolean
  ): ChangePriorityTask {
    const instance: ChangePriorityTask = {
      taskId: id,
      isPriority: !priority,
    };
    return instance;
  }

  public openModal(): void {
    this.open = true;
  }

  public closeModal(state: boolean): void {
    this.open = state;
  }
}
