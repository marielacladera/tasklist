import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Message } from 'primeng/api';
import { take } from 'rxjs/operators';
import { TaskResponse } from 'src/app/responses/task.response';
import { ListTasksService } from 'src/app/services/list-tasks.service';
import { UpdateTaskService } from 'src/app/services/update-task.service';

@Component({
  selector: 'app-form-update-task',
  templateUrl: './form-update-task.component.html',
  styleUrls: ['./form-update-task.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormUpdateTaskComponent implements OnChanges, AfterViewInit {
  @Output() public closedModal: EventEmitter<boolean>;
  @Input() public task!: TaskResponse;

  public minDateValue: Date;
  public newDate: Date;
  public taskEdit!: TaskResponse;

  private readonly _EMPTY: string = '';

  constructor(
    private _updateTaskService: UpdateTaskService,
    private _listTasksService: ListTasksService
  ) {
    this.closedModal = new EventEmitter();
    this.minDateValue = new Date();
    this.newDate = new Date();
    this.taskEdit = {
      taskId: 0,
      title: this._EMPTY,
      description: this._EMPTY,
      isCompleted: false,
      isPriority: false,
      isRemoved: false,
      limitedDateToTask: this._EMPTY,
      createdDate: this._EMPTY,
      updatedDate: this._EMPTY,
    };
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.task.currentValue == this.taskEdit) {
      this.taskEdit = { ...this.task };
    }
  }

  public ngAfterViewInit(): void {
    this.taskEdit = { ...this.task };
    this.newDate = new Date(this.task.limitedDateToTask);
  }

  public operate(): void {
    this._isValid() ? this._updateTask() : this._sendMessageInvalid();
    this.cancelar();
  }

  private _isValid(): boolean {
    return !!this.taskEdit.title && !!this.taskEdit.description;
  }

  private _sendMessageInvalid(): void {
    let message: Message = {
      severity: 'error',
      summary: 'Error',
      detail: `The fields are empty`,
    };
    this._listTasksService.setChangeMessage(message);
  }

  private _updateTask(): void {
    this.taskEdit.limitedDateToTask = this._formatTOISODate();
    this._updateTaskService
      .update(this.taskEdit)
      .pipe(take(1))
      .subscribe(() => {
        this._listTasksService
          .list()
          .pipe(take(1))
          .subscribe(
            (tasks: TaskResponse[]) => {
              const message: Message = {
                severity: 'success',
                summary: 'Success',
                detail: `The task with id ${this.taskEdit.taskId} was to update with exit`,
              };
              this._listTasksService.setChangeTask(tasks);
              this._listTasksService.setChangeMessage(message);
            },
            (err) => {
              this.taskEdit = { ...this.task };
              const message: Message = {
                severity: 'error',
                summary: 'Error',
                detail: 'Cannot update task.',
              };
              this._listTasksService.setChangeMessage(message);
            }
          );
      });
  }

  private _formatTOISODate(): string {
    const tzoffset = new Date().getTimezoneOffset() * 60000;
    const localISOTime: string = new Date(
      this.newDate.getTime() - tzoffset
    ).toISOString();
    return localISOTime;
  }

  public cancelar(): void {
    this.taskEdit = { ...this.task };
    this.closedModal.emit(false);
  }
}
