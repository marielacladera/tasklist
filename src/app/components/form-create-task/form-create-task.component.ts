import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { Message } from 'primeng/api';
import { take } from 'rxjs/operators';
import { CreateRequestTask } from 'src/app/inputs/create-request-task';
import { TaskResponse } from 'src/app/responses/task.response';
import { CreateTaskService } from 'src/app/services/create-task.service';
import { ListTasksService } from 'src/app/services/list-tasks.service';

@Component({
  selector: 'app-form-create-task',
  templateUrl: './form-create-task.component.html',
  styleUrls: ['./form-create-task.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormCreateTaskComponent {
  @Output() public closedFormCreateTask: EventEmitter<boolean>;

  public autoResize: boolean;
  public checked: boolean;
  public description: string;
  public limitedDate: Date;
  public minDateValue: Date;
  public title: string;

  private readonly _EMPTY: string = '';

  constructor(
    private _createTaskService: CreateTaskService,
    private _listTasksService: ListTasksService
  ) {
    this.closedFormCreateTask = new EventEmitter();
    this.description = this._EMPTY;
    this.minDateValue = new Date();
    this.limitedDate = new Date();
    this.title = this._EMPTY;
    this.autoResize = true;
    this.checked = false;
  }

  public operate(): void {
    this._isValid() ? this._registerTask() : this._sendMessageInvalid();
    this.cancel();
  }

  private _isValid(): boolean {
    return !!this.title && !!this.description;
  }

  private _registerTask(): void {
    let createTask: CreateRequestTask = this._createInstanceRequestTask();
    this._createTaskService
      .create(createTask)
      .pipe(take(1))
      .subscribe(() => {
        this._listTasksService
          .list()
          .pipe(take(1))
          .subscribe((tasks: TaskResponse[]) => {
            let message: Message = {
              severity: 'success',
              summary: 'Success',
              detail: `The task was create with exit`,
            };
            this._listTasksService.setChangeTask(tasks);
            this._listTasksService.setChangeMessage(message);
          });
      });
  }

  private _sendMessageInvalid(): void {
    let message: Message = {
      severity: 'error',
      summary: 'Error',
      detail: `The fields are empty`,
    };
    this._listTasksService.setChangeMessage(message);
  }

  private _createInstanceRequestTask(): CreateRequestTask {
    let instance: CreateRequestTask = {
      title: this.title,
      description: this.description,
      isPriority: this.checked,
      limitedDateToTask: this._formatTOISODate(),
    };
    return instance;
  }

  private _formatTOISODate(): string {
    let tzoffset: number = new Date().getTimezoneOffset() * 60000;
    let localISOTime: string = new Date(
      this.limitedDate.getTime() - tzoffset
    ).toISOString();
    return localISOTime;
  }

  public cancel(): void {
    this.closedFormCreateTask.emit(false);
  }
}
