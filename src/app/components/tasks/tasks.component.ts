import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ListTasksService } from 'src/app/services/list-tasks.service';
import { TaskResponse } from '../../responses/task.response';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  providers: [MessageService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TasksComponent implements OnInit, OnDestroy {
  public open: boolean;
  public tasks!: TaskResponse[];

  private _unSubscribeSubject: Subject<void>;

  constructor(
    private _listTasksService: ListTasksService,
    private _messageService: MessageService,
    private _cdr: ChangeDetectorRef
  ) {
    this._unSubscribeSubject = new Subject<void>();
    this.open = false;
  }

  public ngOnInit(): void {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._finalize();
  }

  private _finalize(): void {
    this._unSubscribeSubject.next();
    this._unSubscribeSubject.complete();
  }

  private _listTask(tasks: TaskResponse[]): void {
    this.tasks = tasks;
    this._cdr.markForCheck();
  }

  public isOpen(): void {
    this.open = !this.open;
    this._cdr.markForCheck();
  }

  public cancel(isOpen: boolean): void {
    this.open = isOpen;
    this._cdr.markForCheck();
  }

  private _initialize(): void {
    this._watchChangeMessage();
    this._watchChangeTask();
    this._loadTasks();
  }

  private _watchChangeMessage(): void {
    this._listTasksService
      .getChangeMessage()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((message: Message) => {
        this._messageService.add(message);
        this._cdr.markForCheck();
      });
  }

  private _watchChangeTask(): void {
    this._listTasksService
      .getChangeTask()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((tasks: TaskResponse[]) => {
        this._listTask(tasks);
      });
  }

  private _loadTasks(): void {
    this._listTasksService
      .list()
      .pipe(take(1))
      .subscribe((tasks: TaskResponse[]) => {
        this._listTask(tasks);
      });
  }
}
