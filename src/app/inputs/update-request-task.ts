export interface UpdateRequestTask {
  taskId: number,
  title: string,
  description: string,
  createdDate: string,
  limitedDateToTask: string,
  isPriority: boolean,
  isCompleted: boolean,
  isRemoved: boolean,
}
