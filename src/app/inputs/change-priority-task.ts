export interface ChangePriorityTask {
  taskId: number,
  isPriority: boolean,
}
