import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { TaskComponent } from './components/task/task.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { PrimengModule } from './primeng/primeng.module';
import { FormCreateTaskComponent } from './components/form-create-task/form-create-task.component';
import { FormsModule } from '@angular/forms';
import { FormUpdateTaskComponent } from './components/form-update-task/form-update-task.component';
import { MomentModule } from 'ngx-moment';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TasksComponent,
    FormCreateTaskComponent,
    FormUpdateTaskComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot([]),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MomentModule,
    PrimengModule
  ],
  providers: [{ provide: 'locale', useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
