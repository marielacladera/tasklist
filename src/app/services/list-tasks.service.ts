import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { TaskResponse } from '../responses/task.response';

@Injectable({
  providedIn: 'root'
})
export class ListTasksService {

  private _urlTask: string = `${environment.HOST}/tasks`;
  private _changeTask: Subject<TaskResponse[]>;
  private _changeMessage: Subject<Message>;

  constructor(
    private _http: HttpClient,
  ) {
    this._changeTask = new Subject<TaskResponse[]>();
    this._changeMessage = new Subject<Message>();
   }

  public list(): Observable<TaskResponse[]> {
    return this._http.get<TaskResponse[]>(this._urlTask);
  }

  public getChangeTask(): Observable<TaskResponse []> {
    return this._changeTask.asObservable();
  }

  public setChangeTask(tasks: TaskResponse[]): void{
    this._changeTask.next(tasks);
  }

  public getChangeMessage(): Observable<Message> {
    return this._changeMessage.asObservable();
  }

  public setChangeMessage(message: Message): void {
    this._changeMessage.next(message);
  }
}
