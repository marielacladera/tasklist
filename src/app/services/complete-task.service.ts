import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompleteTaskService {

  private _urlTask: string = `${environment.HOST}/tasks/complete`;

  constructor(
    private _http: HttpClient,
  ) { }

  public completeTask(id: number): Observable<void> {
    return this._http.put<void>(`${this._urlTask}/${id}`, null);
  }

}
