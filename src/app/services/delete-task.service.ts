import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteTaskService {

  private _urlTask: string = `${environment.HOST}/tasks/delete`;

  constructor(
    private _http: HttpClient,
  ) { }


  public delete(id: number): Observable<void> {
    return this._http.put<void>(`${this._urlTask}/${id}`, null);
  }

}
