import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UpdateRequestTask } from '../inputs/update-request-task';

@Injectable({
  providedIn: 'root'
})
export class UpdateTaskService {

  private _urlTask: string = `${environment.HOST}/tasks`;

  constructor(
    private _http: HttpClient
  ) {}

  public update(task: UpdateRequestTask): Observable<Task> {
    return this._http.put<Task>(this._urlTask, task);
  }
}
