import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { CreateRequestTask } from '../inputs/create-request-task';

@Injectable({
  providedIn: 'root'
})
export class CreateTaskService {

  private _urlTask: string = `${environment.HOST}/tasks`;

  constructor(
    private _http: HttpClient,
  ) { }

  public create(task: CreateRequestTask): Observable<Task> {
    return this._http.post<Task>(this._urlTask, task);
  }

}
