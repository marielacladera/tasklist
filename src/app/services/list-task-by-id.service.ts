import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { TaskResponse } from '../responses/task.response';

@Injectable({
  providedIn: 'root'
})
export class ListTaskByIdService {

  private _urlTask: string = `${environment.HOST}/tasks`;

  constructor(
    private _http: HttpClient,
  ) { }

  public listById(id: number): Observable<TaskResponse> {
    return this._http.get<TaskResponse>(`${this._urlTask}/${id}`);
  }

}
