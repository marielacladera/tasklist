import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { ChangePriorityTask } from '../inputs/change-priority-task';

@Injectable({
  providedIn: 'root'
})
export class ChangePriorityTaskService {

  private _urlTask: string = `${environment.HOST}/tasks/priority`;

  constructor(
    private _http: HttpClient,
  ) { }

  public changePriority(change: ChangePriorityTask): Observable<void> {
    return this._http.put<void>(this._urlTask, change);
  }

}
