/**
 * @author Jhonatan Candia
 */
export interface TaskResponse {
  taskId: number;
  title: string;
  description: string;
  createdDate: string;
  updatedDate: string;
  limitedDateToTask: string;
  isPriority: boolean;
  isCompleted: boolean;
  isRemoved: boolean;
}
